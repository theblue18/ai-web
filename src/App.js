import React, { Component } from 'react';
import './App.css';
import TextArea from './TextArea';
import { download } from './utils';
import categories from './categories.json';
import { PREDICT_API } from './constants';
import axios from 'axios';
import { trimNewLine, getSelectionText, checkParentRelation } from './utils';
class App extends Component {
  constructor() {
    super();
    categories.normal = {
      color: '#FFFFFF',
      shortcut: ' ',
    };
    this.state = {
      idx: -1,
      value: undefined,
      
    };
  }

  predict = () => {
    let {
      value,
    } = this.state;
    const newLocal = this;
    axios.post(PREDICT_API, [value]).then((res) => {
      const newData = this.combineChunk(res.data[0][0]);

      value = newData.content;
      let runs = newData.runs;
      

      let currentRuns = 0;
      var index = 0
      const text = newData.content.split('\n').map(trimNewLine).join('\n');
      var keywordArray = []
      Object.keys(runs).map((start) => {
            const { end, type } = runs[start];
            let len = 0;
            const temp = currentRuns;
            currentRuns = end;
            const keyword = text.substring(start, end);
            if(runs[start].type !== "normal")
            {
              if(keywordArray[runs[start].type])
              {
                var flag = true
                var count = keywordArray[runs[start].type].length;
                for(var i = 0; i < count;i++){
                  if(keywordArray[runs[start].type][i].toLowerCase() == keyword.toLowerCase())
                  {
                    flag = false
                  }
                }
                if(flag)
                {
                  keywordArray[runs[start].type][count] = keyword;
                }
                
              }else{

                keywordArray[runs[start].type] = [];
                keywordArray[runs[start].type][0] = keyword;
              }
              
            }
            
      })
      var keywordDictionary = []
      for (var key in keywordArray) {
        for(var i = 0; i < keywordArray[key].length; i ++)
        {
          if(i == 0)
          {
            keywordDictionary[key] = keywordArray[key][i]
          }else{
            keywordDictionary[key] += " , " +keywordArray[key][i]
          }
        }
      }
      newLocal.setState({ value, runs,keywordDictionary });
    });
  }

  combineChunk = (chunks) => {
    const content = chunks.map(i => i[0]).join(' ');
    // console.log(chunks)
    const runs = {};
    let s = 0;
    let p = null;
    chunks.forEach((chunk, idx) => {
      runs[s] = {
        type: chunk[1],
        end: s + chunk[0].length,
        prev: p,
      };
      p = s;
      s += chunk[0].length;
      if (idx < chunks.length - 1) {
        runs[s] = {
          type: 'normal',
          end: s + 1,
          prev: p,
        };
        p = s;
        s += 1;
      }
    });
    s = 0;
    while (runs[s]) {
      const next = runs[runs[s].end];
      if (next && next.type === runs[s].type) {
        const nextNext = runs[next.end];
        const temp = runs[s].end;
        runs[s].end = next.end;
        if (nextNext) {
          nextNext.prev = s;
        }
        delete runs[temp];
      } else {
        s = runs[s].end;
      }
    }
    return { content, runs };
  }

  render() {
    const {
      idx, value, runs, keywordDictionary
    } = this.state;
    return (
      <div className="App container">
       <div>
         <label> AI System Prediction </label>
         </div>
        <textarea 
        className="input-text col-xs-12 col-sm-12 col-md-12 col-lg-12" 
        value={value} 
        onChange={e=>this.setState(
          {value: e.target.value, runs: null}
        )}
        rows={7} />
        <button
          type="button"
          className="btn btn-default"
          key="predict"
          onClick={this.predict}
        >
          Predict
          </button>
        {value ? (
          <TextArea
            key="text-area"
            id={`article-${idx}`}
            text={value}
            categories={categories}
            runs={runs}
            keywordDictionary ={keywordDictionary}
            onSaved={()=>{}}
          />
        ) : null}
      </div>
    );
  }
}

export default App;
